"use strict";

const TripService = require('../src/TripService');
const User = require("../src/User");

describe('TripService', () => {

    let logUser = new User();
    it('should not fail', () => {
        // Given
        const tripService = new StubTripService();
        const user  = new User();
        // When
        const trips = tripService.getTripsByUser(user);

        // Then
        expect(trips).toEqual([]);
    });

    it('should have a friend', () => {
        // Given
        const tripService = new StubTripService();
        const user  = new User();
        const friendUser = new User();
        user.addFriend(friendUser)
        user.addFriend(logUser)

        // When
        const trips = tripService.getTripsByUser(user);

        // Then
        expect(trips).toEqual('toto');
    });

    class StubTripService extends TripService {
        getLogUser() {
            return logUser
        }
    }
});
